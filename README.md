# Innovid IRoll // Integration Example #

This simple example demonstrates how to integrate Innovid iRoll Renderer into your app.
Requires that your channel is using Roku SDK 2.0 (Scenegraph)

## How to run this example
```bash
cd <repo_dir>
make install ROKU_DEV_TARGET=<ROKU_IP> DEVPASSWORD=<ROKU_DEV_PASSWORD>
```

## Integration

#### Add Innovid iRoll Renderer component to your scene xml

**Production** version
```xml
<ComponentLibrary id="_lib"
  uri="https://s-video.innovid.com/common/roku/innovid-iroll-renderer-sg.pkg" />
```

**QA** version
```xml
<ComponentLibrary id="_lib"
  uri="https://s-video.innovid.com/common/roku/innovid-iroll-renderer-sg-qa.pkg" />
```

#### How to create Innovid iRoll instance
As a best practice, you should "prefetch" or "pre-load" the ad by calling the init action ahead of time, and call start when the ad needs to begin.

When calling init, you must pass the channel native resolution setting as the width and height. **It is very important that this is NOT the deviceinfo.getUIresoultion()**

Additionally make sure that before calling start that all playback UI is hidden, as otherwise it may interfere with the interactive user experience
```vbscript

' example or json url - http://video.innovid.com/tags/static.innovid.com/roku/1efrlb.json?cb=659ee1e1-1139-561b-37f1-7688826f630b
' @param String url_       - json file url ( as it defined in VAST )
' @param Float duration_   - duration of iroll ad ( as it defined in VAST )
' @param Float renderTime_ - relative position of iroll ad from beginning of SSAI content
function createIrollAd(url_ as String, duration_ as Float, renderTime_ as Float) as  Void
  m.irollAd = m.top.createChild("InnovidAds:DefaultIrollAd")
  m.irollAd.id = 'iroll-ad'
  m.irollAd.observeFieldScoped("event", "handleIrollEvent")
  m.irollAd.observeFieldScoped("request", "handleIrollPlaybackRequest")
  m.irollAd.action = {
    type   : "init",
    uri    : url_,   ' required!
    width  : 1280,   ' required!
    height : 720,    ' required!
    ssai : {         ' required!
      adapter    : "default",
      renderTime : renderTime_,
      duration   : duration_
    }
  }

  irollAd.action = { type : "start" }
end function
```

#### How to push the playback updates to iRoll instance
The iRoll instance does not listen to the video node events, therefore the host app must provide video playback updates in case of `video.position` or `video.state` change

You can find the sample implementation in [`components/ssai/simple-ssai-video-playback.brs`](https://bitbucket.org/innovid_research/roku-iroll-renderer-sg-example/src/master/components/ssai/simple-ssai-video-playback.brs?at=master&fileviewer=file-view-default#simple-ssai-video-playback.brs-94)

```vbscript
' video.observeFieldScoped("position", "handlePlaybackStateChanged")
' video.observeFieldScoped("state", "handlePlaybackStateChanged")

function handlePlaybackStateChanged(evt_ as Object) as Void
  video_ = evt_.getRoSGNode()

  m.irollAd.action = {
    type     : "notifyPlaybackStateChanged",
    position : video_.position,
    state    : video_.state
  }
end function
```

Also, the host app should handle iRoll playback requests.
you can find the sample implementation in [`components/ssai/iroll-handler.brs`](https://bitbucket.org/innovid_research/roku-iroll-renderer-sg-example/src/master/components/ssai/iroll-handler.brs?at=master&fileviewer=file-view-default#iroll-handler.brs-72)

List of supported requests

| Request                                | Description
| -------------------------------------- | ----------------------------------
| `request-playback-pause`               | pause playback
| `request-playback-resume`              | resume playback
| `request-playback-prepare-to-restart`  | send before iRoll will start a secondary video playback ( common behavior: stop main stream and save position )
| `request-playback-restart`             | send when user closes a microsite and main stream should resume after `request-playback-prepare-to-restart`


```vbscript
' m.irollAd.observeFieldScoped("request", "handleIrollPlaybackRequest")

function handleIrollPlaybackRequest(evt_ as Object) as Void
  request_ = evt_.getData()
  adId_ = evt_.getNode()

  ' request_ : {
  '   type : string,
  '   position? : float ' used in
  ' }

  ? "handleIrollPlaybackRequest(";adId_;", type: ";request_.type;")"

  if request_.type = "request-playback-pause" then
    ' add pause video playback code here
  else if request_.type = "request-playback-resume" then
    ' add resume video playback code here
  else if request_.type = "request-playback-prepare-to-restart" then
    ' this method called before iroll opens a secondary video player
    ' so, the host app should ( in general )
    ' - save a playback position
    ' - completely stop the current player
  else if request_.type = "request-playback-restart" then
    ' this method called after iroll closes a microsite and try to resume ssai playback, the host app should restart a playback from saved position
  end if  
end function
```

#### How to listen to iRoll Ad events
List of supported events

| Event                 | Description                               |
| ----------------------| ----------------------------------------- |
| `Impression`          | Start of ad render (e.g., first frame of a video ad displayed)
| `FirstQuartile`       | 25% of video ad rendered
| `Midpoint`            | 50% of video ad rendered
| `ThirdQuartile`       | 75% of video ad rendered
| `Complete`            | 100% of video ad rendered
| `AcceptInvitation`    | User launched another portion of an ad (for interactive ads)
| `Error`               | Error during ad rendering
| `Expand`              | the user activated a control to expand the creative.
| `Collapse`            | the user activated a control to reduce the creative to its original dimensions.
| `Close`               | User exited out of ad rendering before completion
| `Pause`               | User paused ad
| `Resume`              | User resumed ad
| `Ended`               | Ended
| `exitWithSkipAdBreak` | **TrueX // Pod Skip** when user exits interactive ad unit having earned an ad-free ad break.
| `exitWithAdFree`      | **TrueX // Stream Skip** when user exits interactive ad unit having earned an ad-free stream.
| `exitAutoWatch`       | **TrueX // Auto Advance** when the choice card auto advance timer has expired.
| `exitSelectWatch`     | **TrueX // Select Normal Ads**  when the user selects to watch normal ads.
| `exitBeforeOptIn`     | **TrueX // Close Interactive Ad** when the user exits the `choice_card` with `back` button press


```vbscript
' irollAd.observeFieldScoped("event", "handleIrollEvent")

function handleIrollEvent(evt as Object) as Void
  adEvent = evt.getData()
  adId = evt.getNode()

  ' adEvent : {
  '   type  : string,
  '   data? : object
  ' }

  ? "handleIrollEvent(";adId;", ";adEvent.type;")"
end function
```
